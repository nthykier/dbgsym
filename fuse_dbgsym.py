#!/usr/bin/python3
#

"""
Based on https://github.com/skorokithakis/python-fuse-sample.

Copyright (c) 2019, Niels Thykier
Copyright (c) 2016, Stavros Korokithakis
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import os
import time

import string
import stat
import errno
import logging
import subprocess
import shutil

from fusepy import FUSE, FuseOSError, Operations
import dbgsym.client


LOGGER = logging.getLogger('dbgsym')

FAKE_BUILD_ID_SUBDIR = '_/.build-id/subdir'


def all_hexdigits(s):
    return all(c in string.hexdigits for c in s)


def generic_read_buffer(data, length, offset):
    if isinstance(data, str):
        data = data.encode('utf-8')
    max_read = min(length, len(data) - offset)
    if max_read < 1:
        return ''
    return data[offset:offset + max_read]


class FakePath(object):

    def __init__(self, stat_values, *, dirents=None, contents=None):
        self.stat_values = stat_values
        self._dirents = dirents
        self._contents = contents

    @property
    def dirents(self):
        if self._dirents is None:
            raise FuseOSError(errno.ENOTDIR)
        return self._dirents

    @property
    def contents(self):
        if self._dirents is not None:
            raise FuseOSError(errno.EISDIR)
        return self._contents()

    def read(self, length, offset):
        contents = self.contents
        return generic_read_buffer(contents, length, offset)


class DbgsymFuseFS(Operations):
    def __init__(self, root, download_dir, dbgsym_api_server):
        self.root = os.path.realpath(root)
        self._download_dir = os.path.realpath(download_dir)
        self._dbgsym_api_server = dbgsym_api_server
        self._dbgsym_client = dbgsym.client.DbgsymClient(dbgsym_api_server)

        now = time.time()

        dir_stat = {
            'st_atime': now,
            'st_ctime': now,
            'st_mtime': now,
            'st_uid': 0,
            'st_gid': 0,
            'st_nlink': 2,
            'st_mode': stat.S_IFDIR | 0o0555,
            'st_size': 4096,
        }
        file_stat = {
            'st_atime': now,
            'st_ctime': now,
            'st_mtime': now,
            'st_uid': 0,
            'st_gid': 0,
            'st_nlink': 1,
            'st_mode': stat.S_IFREG | 0o0444,
            # We lie about the size ("justified" by pseudo-files in /sys doing the same)
            'st_size': 4096,
        }
        self.static_paths = {
            '/': FakePath(dir_stat, dirents=[
                '.build-id',
                '.dbgsym-fs-config',
            ]),
            '/.build-id': FakePath(dir_stat, dirents=[]),
            '/.dbgsym-fs-config': FakePath(dir_stat, dirents=[
                'download-dir',
                'dbgsym-api-server',
                'storage-dir',
            ]),
            '/.dbgsym-fs-config/download-dir': FakePath(file_stat, contents=lambda: self._download_dir + "\n"),
            '/.dbgsym-fs-config/dbgsym-api-server': FakePath(file_stat,
                                                             contents=lambda: self._dbgsym_api_server + "\n"
                                                             ),
            '/.dbgsym-fs-config/storage-dir': FakePath(file_stat, contents=lambda: self.root + "\n"),
            FAKE_BUILD_ID_SUBDIR: FakePath(dir_stat, dirents=[]),
        }

    # Helpers
    # =======

    def _full_path(self, partial):
        if partial.startswith("/"):
            partial = partial[1:]
        path = os.path.join(self.root, partial)
        return path

    @staticmethod
    def is_build_id_subdir(path):
        if not path.startswith('/.build-id/'):
            return False
        parts = path.lstrip('/').split('/')
        if len(parts) != 2 or len(parts[1]) != 2 or not all_hexdigits(parts[1]):
            LOGGER.debug("Parts (subdir) are wrong: %d, %s and %s (%s)" % (
                len(parts),
                len(parts[1]) if len(parts) > 1 else 'N/A',
                parts[1] if len(parts) > 1 else 'N/A',
                path,
            ))
            return False
        return True

    # Filesystem methods
    # ==================

    def access(self, path, mode):
        full_path = self._full_path(path)
        if not os.access(full_path, mode):
            raise FuseOSError(errno.EACCES)

    def getattr(self, path, fh=None):
        fake_stat = self.static_paths.get(path)
        if fake_stat is not None:
            return fake_stat.stat_values
        if self.is_build_id_subdir(path):
            return self.static_paths[FAKE_BUILD_ID_SUBDIR].stat_values
        dbgsym_id = self.dbgsym_id_from_path(path)
        if dbgsym_id:
            full_path = self._full_path(path)
            try:
                st = os.lstat(full_path)
            except FileNotFoundError:
                if not self.fetch_dbgsym(dbgsym_id, full_path):
                    raise
                st = os.lstat(full_path)

            return dict((key, getattr(st, key)) for key in ('st_atime', 'st_ctime',
                                                            'st_gid', 'st_mode', 'st_mtime',
                                                            'st_nlink', 'st_size', 'st_uid')
                        )
        raise FuseOSError(errno.ENOENT)

    def readdir(self, path, fh):
        dirents = ['.', '..']
        fake_path = self.static_paths.get(path)
        if fake_path:
            for extra_path in fake_path.dirents:
                dirents.append(extra_path)

        if path == '/.build-id':
            full_path = self._full_path(path)
            dirents.extend(x for x in os.listdir(full_path) if len(x) == 2 and all_hexdigits(x))
        elif self.is_build_id_subdir(path):
            full_path = self._full_path(path)
            dirents.extend(x for x in os.listdir(full_path) if self.dbgsym_id_from_path(os.path.join(path, x)))

        yield from dirents

    # File methods
    # ============

    def open(self, path, flags):
        if flags & 3 != os.O_RDONLY:
            raise FuseOSError(errno.EROFS)
        dbgsym_id = self.dbgsym_id_from_path(path)
        fake_path = self.static_paths.get(path)
        if not dbgsym_id:
            if self.is_build_id_subdir(path):
                raise FuseOSError(errno.EISDIR)
            if fake_path and fake_path.contents:
                return 0
            raise FuseOSError(errno.ENOENT)

        full_path = self._full_path(path)
        try:
            fd = os.open(full_path, flags)
        except FileNotFoundError:
            if dbgsym_id and self.fetch_dbgsym(dbgsym_id, full_path):
                LOGGER.info('Fetched %s successfully into %s', dbgsym_id, full_path)
                fd = os.open(full_path, flags)
            else:
                raise
        return fd

    def read(self, path, length, offset, fh):
        LOGGER.debug("read %s", path)
        fake_path = self.static_paths.get(path)
        if fake_path:
            return fake_path.read(length, offset)
        os.lseek(fh, offset, os.SEEK_SET)
        return os.read(fh, length)

    def release(self, path, fh):
        return os.close(fh)

    @staticmethod
    def dbgsym_id_from_path(path):
        base_parts = os.path.splitext(path)
        if len(base_parts) != 2 or base_parts[1] != '.debug':
            LOGGER.debug("Parts are wrong: %d and %s" % (
                len(base_parts),
                base_parts[1] if len(base_parts) > 1 else 'N/A'
            ))
            return None
        base_path = base_parts[0].lstrip('/')
        if not base_path.startswith('.build-id/') and not base_path.startswith('.dwz/'):
            LOGGER.debug("Prefix is wrong: %s" % base_path)
            return None
        parts = base_path.split('/')
        if len(parts) != 3:
            LOGGER.debug("Parts (subdir) are wrong: %d, %s and %s (%s)" % (
                len(parts),
                parts[1] if len(parts) > 1 else 'N/A',
                parts[2] if len(parts) > 2 else 'N/A',
                path,
            ))
            return None
        if parts[0] == '.dwz':
            # FIXME
            return None
        elif len(parts[1]) == 2 and len(parts[2]) == 38:
            digest = parts[1] + parts[2]
            LOGGER.debug("Maybe digest: %s", digest)
            if all_hexdigits(digest):
                return digest
        LOGGER.debug("Not a dbgsym file")
        return None

    def fetch_dbgsym(self, dbgsym_id, full_path):
        LOGGER.warning("Need to fetch %s into %s" % (dbgsym_id, full_path))
        dl_files = self._dbgsym_client.lookup_dbgsym(dbgsym_id)
        if not dl_files:
            return False
        total_files = len(dl_files)
        dl_files = [x for x in dl_files if 'sha256' in x.checksums]
        if not dl_files:
            LOGGER.info("None of the files for %s had a SHA256 checksum", dbgsym_id)
            return False
        download_path = None
        for dl_file in dl_files:
            try:
                download_path = self._dbgsym_client.download_file(dl_file, self._download_dir)
            except dbgsym.client.MissingRequiredChecksumError:
                LOGGER.info("Failed to download %s (build-id: %s): Missing strong checksum", dl_file.name, dbgsym_id)
                return False
            except dbgsym.client.NoFilesSuccessfullyDownloadableError:
                LOGGER.info("Failed to download %s for %s", dl_file.name, dbgsym_id)
                continue
        if not download_path:
            if total_files != len(dl_files):
                LOGGER.info("Could not download %s due to errors or/and missing SHA256 checksums", dbgsym_id)
            else:
                LOGGER.info("Could not download %s due to errors", dbgsym_id)
            return False
        unpack_dir = download_path + ".unpacked"

        shutil.rmtree(unpack_dir, ignore_errors=True)
        try:
            subprocess.check_call(['dpkg-deb', '-x', download_path, unpack_dir])
        except subprocess.CalledProcessError as e:
            LOGGER.warning("Extraction of %s failed: %s", download_path, str(e))
            LOGGER.warning("Try dpkg-deb -x \"%s\" \"unpack_dir\" for more information", download_path, unpack_dir)
            shutil.rmtree(unpack_dir, ignore_errors=True)
            return False

        LOGGER.info("Successfully extracted %s", download_path)

        debug_root_dir = os.path.join(unpack_dir, 'usr', 'lib', 'debug', '.build-id')
        target_debug_dir = os.path.join(self.root, '.build-id')
        has_id = False
        for current_dir, subdirs, files in os.walk(debug_root_dir):
            LOGGER.debug("os.walk yield (%s, %s, %s)", current_dir, str(subdirs), str(files))
            if current_dir == debug_root_dir:
                to_keep = [x for x in subdirs if len(x) == 2 and all_hexdigits(x)]
                if len(to_keep) != len(subdirs):
                    LOGGER.debug("Pruning subdirs list of .build-id dir from %s (original: %s)",
                                 download_path, str(subdirs))
                    subdirs.clear()
                    subdirs.extend(to_keep)
                    LOGGER.debug("New subdirs list of .build-id dir from %s: %s", download_path, str(subdirs))
                for subdir in subdirs:
                    d = os.path.join(target_debug_dir, subdir)
                    LOGGER.debug("Ensuring that %s exists", d)
                    os.makedirs(d, exist_ok=True)
            elif current_dir[-3] == '/' and all_hexdigits(current_dir[-2:]):
                subdirs.clear()
                subdir_basename = current_dir[-2:]
                target_dir = os.path.join(target_debug_dir, subdir_basename)
                for f in files:
                    if not f.endswith('.debug'):
                        LOGGER.info("Ignoring unknown file beneath %s in the .build-id dir of the deb", current_dir)
                        continue
                    base_parts = os.path.splitext(f)
                    if len(base_parts) != 2 or len(base_parts[0]) != 38 or not all_hexdigits(base_parts[0]):
                        LOGGER.info("Ignoring unknown .debug file beneath %s in the .build-id dir of the deb",
                                    current_dir)
                        continue
                    shutil.copyfile(os.path.join(current_dir, f), os.path.join(target_dir, f + ".tmp"))
                    os.rename(os.path.join(target_dir, f + ".tmp"), os.path.join(target_dir, f))
                    fetched_digest = subdir_basename + base_parts[0]
                    LOGGER.info("The build-id %s is now available", fetched_digest)
                    if fetched_digest == dbgsym_id:
                        has_id = True

            else:
                LOGGER.info("Ignoring unknown path %s in the .build-id dir of the deb", current_dir)
                subdirs.clear()

        shutil.rmtree(unpack_dir, ignore_errors=True)
        if not has_id:
            LOGGER.warning("The container did not include the dbgsym %s like the API server claimed!?", dbgsym_id)
        return has_id


def main(mountpoint, download_dir, root, dbgsym_api_server, allow_other):
    logging.basicConfig(format='[{asctime}] {levelname}: {message}',
                        style='{',
                        datefmt="%Y-%m-%dT%H:%M:%S%z",
                        level=logging.INFO)
    FUSE(DbgsymFuseFS(root, download_dir, dbgsym_api_server), mountpoint,
         nothreads=True, foreground=True, allow_other=allow_other)


if __name__ == '__main__':
    import argparse

    description = """\
This tool provides a read-only file system of debug symbols used by (e.g.) gdb.  It fetches debug symbols
from the network sources, extracts them and makes them available on the fly.  Example:

  mkdir -p dbgsym-cache download-cache mountpoint
  %(prog) dbgsym-cache download-cache mountpoint
  ls -l mountpoint/.build-id/c4/e26d25e8921c8ab71d496d1832f4fa6fd93b97.debug

Assuming the dbgsym-api-server (change with --dbgsym-api-serer) is available and can tell us where to
fetch the debug symbol for build-id c4e26d25e8921c8ab71d496d1832f4fa6fd93b97, then the ls command will
show you a file.

"""

    epilog = """\

Running this tool as root (or another administrator-like user) is *not* recommended as it is a network
based service.
"""
    parser = argparse.ArgumentParser(allow_abbrev=False, epilog=epilog)
    parser.add_argument('backing-store-dir', help="Writable directory used to store the fetched dbgsym files")
    parser.add_argument('download-cache-dir', help="Writable cache directory for download and extraction of files")
    parser.add_argument('mountpoint', help="Desired mountpoint (must be an empty directory)")
    parser.add_argument('--dbgsym-api-server', dest='dbgsym_api_server', default='http://127.0.0.1:8000',
                        help="URL to the dbgsym API server.")
    parser.add_argument('--allow-other', dest='allow_other', action='store_true',
                        help="Allow other users to access the mountpoint."
                             "Described in fuse(5) (\"allow_other\").  This option may require running the command"
                             " as root or an update of /etc/fuse.conf"
                        )
    parser.add_argument('--no-allow-other', dest='allow_other', action='store_false',
                        help="Prevent other users from accessing the mountpoint. "
                             "Described in fuse(5) (\"allow_other\")"
                        )
    args = parser.parse_args()

    main(args.mountpoint,
         getattr(args, 'download-cache-dir'),
         getattr(args, 'backing-store-dir'),
         args.dbgsym_api_server,
         args.allow_other,
         )
