import logging
import requests
import typing
import os
import hashlib
import tempfile


DOWNLOADABLE_FILES_REQUIRED_FIELDS = ['name', 'file_type', 'build_ids', 'file_size', 'download_urls']
DOWNLOADABLE_FILES_OPTIONAL_FIELDS = {'checksums': list}
DOWNLOADABLE_FILES_ALL_FIELDS = DOWNLOADABLE_FILES_REQUIRED_FIELDS.copy()
DOWNLOADABLE_FILES_ALL_FIELDS.extend(DOWNLOADABLE_FILES_OPTIONAL_FIELDS)
CHUNK_SIZE = 1048576  # 1 MB
DBGSYM_CLIENT_VERSION = '0.0.1'


class DbgsymRequiredFieldError(RuntimeError):
    pass


class DbgsymAPIError(RuntimeError):
    pass


class DbgsymFileDownloadException(RuntimeError):
    pass


class MissingRequiredChecksumError(DbgsymFileDownloadException):
    pass


class NoFilesSuccessfullyDownloadableError(DbgsymFileDownloadException):
    pass


class DLFileUrl(object):

    __slots__ = ['url', 'expiry']

    def __init__(self, url=None, expiry=None):
        self.url = url
        self.expiry = expiry
        if url is None:
            raise DbgsymRequiredFieldError('url')


class DLFile(object):

    __slots__ = DOWNLOADABLE_FILES_ALL_FIELDS

    def __init__(self, name=None, file_type=None, build_ids=None, file_size=None,
                 download_urls=None, checksums=None):
        self.name: str = name
        self.file_type: str = file_type
        self.build_ids: list = build_ids
        self.file_size = file_size
        self.download_urls: typing.List[DLFileUrl] = download_urls
        self.checksums: dict = checksums


class DbgsymClient(object):
    def __init__(self, api_host):
        self._api_host = api_host
        self._logger = logging.getLogger(".".join((self.__class__.__module__, self.__class__.__name__)))
        self._chunk_size = CHUNK_SIZE
        self._user_agent = 'dbgsym-client v%s' % DBGSYM_CLIENT_VERSION

    @property
    def chunk_size(self):
        return self._chunk_size

    @chunk_size.setter
    def chunk_size(self, new_value):
        if new_value < 1:
            raise ValueError("chunk_size must be >= 1")
        self._chunk_size = new_value

    def lookup_dbgsym(self, build_id: str) -> typing.List[DLFile]:
        host = self._api_host
        logger = self._logger
        all_files = []
        url = "%s/api/v1/find-dbgsym/%s/" % (host, build_id)
        logger.info('Requesting download information about %s from API server %s',
                    build_id, host)
        try:
            r = requests.get(url, headers={'User-Agent': self._user_agent})
        except requests.exceptions.ConnectionError as e:
            logger.warning("Failed to connect to API server %s failed: %s", host, str(e))
            raise
        except requests.exceptions.RequestException as e:
            logger.warning("Request for %s from API server %s failed: %s", build_id, host, str(e))
            raise
        try:
            data = r.json()
        except RuntimeError as e:
            logger.warning("Failed to unpack response payload fromAPI server %s failed: %s", host, str(e))
            raise

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug('Requested %s, HTTP STATUS %d', url, r.status_code)
            logger.debug('Response payload %s', data)

        if r.status_code == 503 or r.status_code == 429:
            logger.info('API server is currently having issues or rate limited us (HTTP %d from %s)',
                        r.status_code, host)
            raise DbgsymAPIError('API server is currently having issues or rate limited us (HTTP %d from %s)' % (
                                 r.status_code, host))
        if r.status_code >= 500:
            logger.info('API server is having issues (HTTP %d from %s)', r.status_code, host)
            raise DbgsymAPIError('API server is having issues (HTTP %d from %s)' % (
                r.status_code, host))
        if r.status_code == 404:
            logger.info('API server does not know %s (HTTP 404 from %s)', build_id, host)
            return all_files
        if r.status_code != 200:
            logger.warning('API server returned unexpected status code %d when asked about %s (API server: %s)',
                           r.status_code,
                           build_id,
                           host
                           )
            raise DbgsymAPIError('API server returned unexpected status code %d when asked about %s (API server: %s)' %
                                 (r.status_code, build_id, host))

        files_list = data.get('files')
        if files_list is None:
            logger.warning('API server returned unexpected result on %s - missing mandatory field %s (API server: %s)',
                           build_id,
                           'files',
                           host
                           )
        for i, file_entry in enumerate(files_list):
            for field in DOWNLOADABLE_FILES_REQUIRED_FIELDS:
                if field not in file_entry:
                    logger.warning('API server returned unexpected result on %s - missing mandatory field files[%d].%s"'
                                   ' (API server: %s): Skipping entry.',
                                   i,
                                   build_id,
                                   field,
                                   host
                                   )
                continue
            for field, factory_function in DOWNLOADABLE_FILES_OPTIONAL_FIELDS.items():
                if field not in file_entry:
                    file_entry[field] = factory_function()

            try:
                file_entry['download_urls'] = [DLFileUrl(**x) for x in file_entry['download_urls']]
            except DbgsymRequiredFieldError as e:
                logger.warning('API server returned unexpected result on %s - missing mandatory field files[%d].url.%s'
                               '- ignoring entry the response (API server: %s)',
                               i,
                               e.args[0],
                               build_id,
                               host
                               )
            dl_file = DLFile(**file_entry)
            if build_id not in dl_file.build_ids:
                logger.warning('API server returned a reply that did not include %s in files[%d] - ignoring entry the '
                               'response (API server: %s)',
                               i,
                               build_id,
                               host
                               )
                continue
            if '/' in dl_file.name or '..' in dl_file.name or '\\' in dl_file.name:
                logger.warning('API server returned a reply that included a questionable file name in files[%d].name '
                               'for %s - ignoring entry the response (API server: %s)',
                               i,
                               build_id,
                               host
                               )
                continue
            all_files.append(dl_file)
        return all_files

    def _verify_checksum(self, path, expected_checksum):
        computed = hashlib.sha256()
        chunk_size = self.chunk_size
        with open(path, 'rb') as fd:
            buf = fd.read(chunk_size)
            computed.update(buf)
        if computed.hexdigest() == expected_checksum:
            return True
        return False

    def _verify_file(self, path, dl_file, require_checksum):
        st = os.stat(path)
        if st.st_size != dl_file.file_size:
            return False
        if require_checksum and not self._verify_checksum(path, dl_file.checksums['sha256']):
            return False
        return True

    def download_file(self, dl_file: DLFile, target_dir,
                      require_checksum=frozenset(['deb', 'udeb'])):
        basename = dl_file.name
        path = os.path.join(target_dir, basename)
        logger = self._logger
        chunk_size = self.chunk_size
        if type(require_checksum) == bool:
            need_checksum = require_checksum
        elif callable(require_checksum):
            need_checksum = require_checksum(dl_file)
        elif dl_file.file_type in require_checksum:
            need_checksum = True
        else:
            need_checksum = False

        logger.debug("Request to download %s, checksum requirement: %s", dl_file.name, str(need_checksum))
        if need_checksum and 'sha256' not in dl_file.checksums:
            raise MissingRequiredChecksumError("Required (strong) checksum for %s but it had no sha256 checksum" %
                                               basename)
        try:
            if self._verify_file(path, dl_file, need_checksum):
                logger.info('[CACHED]: %s was already downloaded (size + checksum matched)', basename)
                return path
        except FileNotFoundError:
            pass

        dl_fd = tempfile.NamedTemporaryFile(dir=target_dir, prefix='tmp.dl.%s.' % basename, suffix='.bin', delete=False)
        rename_successful = False
        try:
            download_path = dl_fd.name

            for dl_file_url in dl_file.download_urls:
                logger.info("[STARTING-DOWNLOAD]: Attempting to download %s from %s (size: %s MB)",
                            basename, dl_file_url.url, round(dl_file.file_size/(1024.0 * 1024.0), 2))
                try:
                    r = requests.get(dl_file_url.url, stream=True, headers={'User-Agent': self._user_agent})
                except requests.exceptions.ConnectionError as e:
                    logger.warning("Failed to connect to %s (trying to download %s): %s",
                                   dl_file_url.url,
                                   basename,
                                   str(e)
                                   )
                    continue
                except requests.exceptions.RequestException as e:
                    logger.warning("Failed to fetch %s from %s: %s", basename, dl_file_url.url, str(e))
                    continue

                dl_fd.truncate(0)
                dl_fd.seek(0)
                for chunk in r.iter_content(chunk_size=chunk_size):
                    dl_fd.write(chunk)
                dl_fd.flush()

                logger.info("[FETCH-COMPLETE]: Downloaded (pre-verification) %s from %s",
                            basename, dl_file_url.url)
                if self._verify_file(download_path, dl_file, need_checksum):
                    logger.info("[DOWNLOAD-OK]: Downloaded %s successfully from %s",
                                basename, dl_file_url.url)
                    os.rename(download_path, path)
                    rename_successful = True
                    return path
                else:
                    logger.info("[BAD-DOWNLOAD]: Downloaded file %s from %s had incorrect size or checksum; discarding",
                                basename, dl_file_url.url)

            raise NoFilesSuccessfullyDownloadableError(dl_file.name)
        finally:
            if not rename_successful:
                os.unlink(dl_fd.name)

