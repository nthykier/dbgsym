# REST service to answer "which Debian package provides the build-id I am missing"?

This is the code base of a small REST service that map a build-id into
a Debian package, so you can fetch it from snapshot.debian.org.

## Getting started

The basic steps are:

    # ONLY step requiring root
    apt-get build-dep .

    # In the shell of a regular user
    export PGHOST="$(pwd)/local_db"
    PG_VERSION=11
    mkdir -p "$PGHOST"
    /usr/lib/postgresql/$PG_VERSION/bin/initdb -k "$PGHOST"
    echo "unix_socket_directories = '$PGHOST'" >> "$PGHOST/postgresql.conf"
    echo "listen_addresses = ''" >> "$PGHOST/postgresql.conf"
    # In case you want to enable POWA for db performance debugging
    echo "shared_preload_libraries='pg_stat_statements,powa,pg_stat_kcache,pg_qualstats'" >> "$PGHOST/postgresql.conf"

    # Start the database server
    /usr/lib/postgresql/$PG_VERSION/bin/pg_ctl start -D "$PGHOST" -l "$(pwd)/postgres-startup.log"

    echo 'CREATE DATABASE dbgsym;' | psql postgres

    python3 manage.py migrate
    ./import_sample_data.py
    python3 manage.py runserver

At this point, you should be able to fetch a result from the service via:

    curl --silent http://127.0.0.1:8000/api/v1/find-dbgsym/09fadc692456a45d46004240bc9bd16090789d5d/ | python -m json.tool

Or use the sample client:

    rm -fr downloads && mkdir downloads
    ./sample_client.py http://127.0.0.1:8000 09fadc692456a45d46004240bc9bd16090789d5d downloads


Note the first calls to a given build-id may be slow as the webservice
will pull missing data (e.g. the download url) from
snapshot.debian.org.  The data will be pulled lazily (i.e. when we
need it) and then committed to the database to speed up future
request.


## Resetting the data in the database

In case you want to reset the database, the following steps can be used:

     echo 'DROP DATABASE dbgsym; CREATE DATABASE dbgsym;' | psql postgres
     python3 manage.py migrate
     # Import data again (e.g. ./import_sample_data.py)


## Resetting the database completely

Basically (assumes you have set the ENV vars from the `Getting started` section:

     /usr/lib/postgresql/$PG_VERSION/bin/pg_ctl stop -D "$PGHOST" -l "$(pwd)/postgres-startup.log"
     rm -fr "$PGHOST"

After this, you will have to follow the steps in `Getting started` to
re-create the database.


## Setting up POWA for database performance debugging

First a bit of setup:

    echo 'CREATE DATABASE powa;' | psql postgres
    for extension in pg_stat_statements btree_gist powa pg_qualstats pg_stat_kcache ; do
        echo "CREATE EXTENSION IF NOT EXISTS $extension;"
    done | psql powa
    echo 'CREATE EXTENSION hypopg;' | psql dbgsym

    apt-get install python3-pip python3-sqlalchemy python3-tornado
    pip3 install powa-web
    PGUSER=$(whoami) perl -p -E 's/@(PGHOST|PGUSER)@/$ENV{$1}/ge;' powa-web.conf.tmpl > powa-web.conf

At this point, we can start the web service with:

    ~/.local/bin/powa-web

You may want to run that in a separate terminal (but remember to `cd`
to this project as it looks for the config in the current working
directory).

Now perform the task that puts load on the database and direct your
browser to http://127.0.0.1:8888 to see what POWA finds.

Note that POWA may take a while (a la 5 minutes) between snapshots.
You may want to sustain the load for a while and then find something
useful to do while you wait.

# Tools

The following sample tools are available for consuming the API:

 * `./sample_client.py`: Simple tool to lookup build-ids via the dbgsym API server and then fetch the
   relevant debs.  This tool requires `python3-requests`.
 * `./fuse_dbgsym.py`: Tool to create a FUSE mountpoint that automatically fetches missing build-ids by
   asking the dbgsym API server.  This tool requires `python3-fusepy` to work in addition to the
   dependencies required by `sample_client.py`.

The `sample_client.py` tool can be used in conjunction with `fuse_dbgsym.py` to "warm up" the FUSE mount
by downloading debs into the denoted cache directory.
