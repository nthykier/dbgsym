#!/usr/bin/python3

import sys


import dbgsym.client


def usage():
    print("""Usage %(prog)s <dbgsym-host> <build-id>[ ... <build-id>] <download-dir>

Example:
      mkdir -p downloads
      %(prog)s http://127.0.0.1:8000 09fadc692456a45d46004240bc9bd16090789d5d downloads
""")


def main():
    if len(sys.argv) < 4 or (len(sys.argv) > 1 and sys.argv[1] in {'-h', '--help'}):
        usage()
        sys.exit(0)

    dbgsym_host = sys.argv[1]
    download_dir = sys.argv.pop()
    build_ids = set(sys.argv[2:])
    to_download = []
    no_download_url = []
    unknown_build_ids = []
    dbgsym_client = dbgsym.client.DbgsymClient(dbgsym_host)
    while build_ids:
        build_id = build_ids.pop()
        dl_files = dbgsym_client.lookup_dbgsym(build_id)
        if not dl_files:
            unknown_build_ids.append(build_id)
            continue
        downloadable = [x for x in dl_files if x.download_urls]
        if not downloadable:
            continue
        best = dl_files[0]
        most_extra_in_common = frozenset()
        for dl_file in downloadable:
            in_common = build_ids.intersection(dl_file.build_ids)
            if len(in_common) > len(most_extra_in_common):
                best = dl_file
                most_extra_in_common = in_common
        print("Choosing to download %s to get %s.  It includes %d unique missing build-ids." % (
            best.name, build_id, len(most_extra_in_common) + 1))
        build_ids -= most_extra_in_common
        to_download.append(best)

    if to_download:
        print("Starting download of %d packages" % len(to_download))
        for pkg in to_download:
            dbgsym_client.download_file(pkg, download_dir)
        print("Download complete")
        print()

    print("Summary:")
    if to_download:
        print("    [OK]: Fetched %d files into %s (of total size: %d MB)" % (
            len(to_download), download_dir, round(sum(x.file_size for x in to_download)/(1024.0 * 1024.0), 2)))
    if no_download_url:
        print("    [NOT-DOWNLOADABLE]: Found %d files without download urls (of total size: %d MB)" % (
            len(no_download_url), round(sum(x.file_size for x in no_download_url)/(1024.0 * 1024.0), 2)))
    if unknown_build_ids:
        print("    [UNKNOWN]: Found %d unknown build-ids" % len(unknown_build_ids))


if __name__ == '__main__':
    main()
