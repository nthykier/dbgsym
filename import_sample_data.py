#!/usr/bin/python3

import os

os.environ['DJANGO_SETTINGS_MODULE'] = 'dbgsym_project.settings'
os.environ['DBGSYM_API_APP_DEBUG'] = 'false'


import django
django.setup()


from dbgsym_api_app.models import BuildId, HashType, FileHash, DownloadableFileType, DownloadableFile, DownloadUrl, Vendor


def get_or_create(cls, **kwargs):
    return cls.objects.get_or_create(**kwargs)[0]


hash_type_sha1 = get_or_create(HashType, hash_name='sha1')
hash_type_sha256 = get_or_create(HashType, hash_name='sha256')
file_type = get_or_create(DownloadableFileType, file_type='deb')
vendor = get_or_create(Vendor, vendor_name='Debian')

mscgen_dbgsym_pkg = get_or_create(DownloadableFile, filename='mscgen-dbgsym_0.20-12_amd64.deb', file_type=file_type,
                                  vendor=vendor, defaults={'file_size': 87908})
t1utils_dbgsym_pkg = get_or_create(DownloadableFile, filename='t1utils-dbgsym_1.41-3_amd64.deb', file_type=file_type,
                                   vendor=vendor, defaults={'file_size': 23400})

base_url = 'https://snapshot.debian.org/archive/debian-debug'

pkg_hashes = (
    (mscgen_dbgsym_pkg, [
        get_or_create(FileHash, hash_type=hash_type_sha256,
                      hash_value='adf51b46a849f5a69ee25ec4669997ef1c8f53ab5adcb15c6af805b1e06274a3'),
        get_or_create(FileHash, hash_type=hash_type_sha1, hash_value='2146040f7737634decbfde84db0c9564297bd754'),
    ], [
        'c4e26d25e8921c8ab71d496d1832f4fa6fd93b97'
    ], [
        base_url + '/20181224T220556Z/pool/main/m/mscgen/mscgen-dbgsym_0.20-12_amd64.deb'
    ]),
    (t1utils_dbgsym_pkg, [
        get_or_create(FileHash, hash_type=hash_type_sha256,
                      hash_value='ed91885292cd8e98f4760ee26c80f78d4e3d9a6950864480833a4c5004401109'),
        get_or_create(FileHash, hash_type=hash_type_sha1, hash_value='d5aca6ed6f2aa111e3257f52eb7696a73bc2ac68'),
    ], [
        '09fadc692456a45d46004240bc9bd16090789d5d',
        '0e912a6f8c2e29aeabde8b327588ef89d5a25430',
        '2e7a6523c1fd9584d649dbe876989ec269ffb008',
        '59c98376a2f90319e504563d2f1b26956ee87dd5',
        'ca0ce5a66818304f959c58b3863bfdb66f09295b',
        'cb146d8fada31c4df8c0171fc8b03abb59471496',
    ], [
        base_url + '/20181225T145219Z/pool/main/t/t1utils/t1utils-dbgsym_1.41-3_amd64.deb'
    ]),
)

for dl_file, phashes, build_ids, download_urls in pkg_hashes:
    dl_file.file_hashes.set(phashes)
    dl_file.build_ids.set([get_or_create(BuildId, build_id=x) for x in build_ids])
    for download_url in download_urls:
        get_or_create(DownloadUrl, url=download_url, downloadable_file=dl_file)
    dl_file.save()

help_args = {
    'dbgsym_id': t1utils_dbgsym_pkg.build_ids.all().first()
}
    

print("""Data imported successfully

Now try to run (if it is not already running):

  DBGSYM_API_APP_DEBUG=true python3 manage.py runserver

Followed by (in another window):

  curl --silent http://127.0.0.1:8000/api/v1/find-dbgsym/%(dbgsym_id)s/ | python -m json.tool
""" % help_args)

