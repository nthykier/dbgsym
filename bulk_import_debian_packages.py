#!/usr/bin/python3

import apt_pkg
import collections
import os
import sys
import datetime

os.environ['DJANGO_SETTINGS_MODULE'] = 'dbgsym_project.settings'
os.environ['DBGSYM_API_APP_DEBUG'] = 'false'

import django
django.setup()


from dbgsym_api_app.models import BuildId, HashType, FileHash, DownloadableFile, DownloadableFileType, Vendor, DownloadUrl
from dbgsym_api_app.utils import fetch_or_create, bulk_import_hash
from django.db import transaction

apt_pkg.init()


PROGRESS_AFTER = 1000
FLUSH_AFTER = 5000


class DBCache(collections.defaultdict):

    def __init__(self, db_cls, db_field_name):
        super().__init__()
        self._db_cls = db_cls
        self._db_field_name = db_field_name

    def __missing__(self, key):
        kwargs = {self._db_field_name: key}
        value = fetch_or_create(self._db_cls, **kwargs)
        self[key] = value
        return value


HASH_TYPE_CACHE = DBCache(HashType, 'hash_name')
FILE_TYPE_CACHE = DBCache(DownloadableFileType, 'file_type')
VENDOR_CACHE = DBCache(Vendor, 'vendor_name')


def bulk_import_pkgs(cls, vendor, file_type, items):
    new_pkgs = []
    assert items

    filenames = [item['filename'] for item in items]
    query = cls.objects.filter(vendor=vendor, file_type=file_type, filename__in=filenames).\
        prefetch_related('downloadurl_set')
    existing_files = {e.filename: e for e in query}

    to_create = []
    existing_pkgs = []
    for item in items:
        filename = item['filename']

        if filename in existing_files:
            existing_pkgs.append((item, existing_files[filename]))
            continue
        entry = cls(filename=filename,
                    vendor=vendor,
                    file_type=file_type,
                    file_size=item['file_size']
                    )
        new_pkgs.append((item, entry))
        to_create.append(entry)
    cls.objects.bulk_create(to_create)
    return new_pkgs, existing_pkgs


# Use a transaction to batch changes (i.e. work around "autocommit on each change").
@transaction.atomic
def flush_work_list(all_build_ids, all_sha256_pkg_hashes, worklist, vendor, file_type, expiry):
    build_ids = bulk_import_hash(BuildId, all_build_ids, 'build_id')
    sha256_hashes = bulk_import_hash(FileHash, all_sha256_pkg_hashes, 'hash_value',
                                     create_args={'hash_type': HASH_TYPE_CACHE['sha256']},
                                     filter_args={'hash_type__exact': HASH_TYPE_CACHE['sha256']})
    new_pkgs, existing_pkgs = bulk_import_pkgs(DownloadableFile, vendor, file_type, worklist)

    for item, entry in new_pkgs:
        entry.file_hashes.add(sha256_hashes[item['sha256']])
        entry.build_ids.set([build_ids[x] for x in item['build_ids']])
        download_url, created = DownloadUrl.objects.get_or_create(url=item['url'],
                                                                  downloadable_file=entry,
                                                                  defaults={'expiry': expiry})
        if not created:
            download_url.expiry = expiry
            download_url.save()
        entry.downloadurl_set.add(download_url)

    for item, entry in existing_pkgs:
        download_url, created = DownloadUrl.objects.get_or_create(url=item['url'],
                                                                  downloadable_file=entry,
                                                                  defaults={'expiry': expiry})
        if not created:
            download_url.expiry = expiry
            download_url.save()
        if download_url not in entry.downloadurl_set.all():
            entry.downloadurl_set.add(download_url)


def import_file(vendor_name, external_base_url, expiry, path):
    t = apt_pkg.TagFile(path)
    c = 0
    print("Starting import of %s" % path)
    time_start = datetime.datetime.now()
    time_last = time_start
    worklist = []
    all_build_ids = set()
    all_pkg_hashes = set()
    file_type = FILE_TYPE_CACHE['deb']
    vendor = VENDOR_CACHE[vendor_name]

    for section in t:
        if 'Build-Ids' not in section:
            continue
        build_ids = section['Build-Ids'].split()
        filename = section['Filename']
        url = "%s/%s" % (external_base_url, filename)
        basename = os.path.basename(section['Filename'])
        all_pkg_hashes.add(section['SHA256'])
        item = {
            'sha256': section['SHA256'],
            'file_size': int(section['Size']),
            'build_ids': build_ids,
            'filename': basename,
            'url': url,
        }
        all_build_ids.update(build_ids)
        worklist.append(item)
        c += 1
        if c % FLUSH_AFTER == 0:
            print("Flushing data to the database (at %d entries)" % c)
            flush_work_list(all_build_ids, all_pkg_hashes, worklist, vendor, file_type, expiry)
            all_build_ids = set()
            all_pkg_hashes = set()
            worklist = []
        if c % PROGRESS_AFTER == 0:
            now = datetime.datetime.now()
            duration = now - time_last
            print("Processed %d entries in %s (took: %s)" % (c, path, duration))
            time_last = now
    if worklist:
        print("Flushing data to the database")
        flush_work_list(all_build_ids, all_pkg_hashes, worklist, vendor, file_type, expiry)

    now = datetime.datetime.now()
    duration = now - time_start
    print("Processing all entries from %s took %s" % (path, duration))


if __name__ == '__main__':
    import argparse

    description = """\
Tool to bulk import package information and build-ids from one or more
Debian Packages file.  It will reset the relations of packages and
their build-ids to match data processed (if the same package appears
in multiple packages-files they are processed in order and the last
one is used).
"""

    parser = argparse.ArgumentParser(allow_abbrev=False, description=description)
    parser.add_argument('packages_files', nargs='+', help="Packages files to process")
    parser.add_argument('--vendor', type=str, default='Debian',
                        help="Vendor (e.g. Debian, Ubuntu, your-company-here) providing the Packages files."
                        )
    parser.add_argument('--mirror-external-base-url', dest='external_base_url', type=str, required=True,
                        help="External base URL for the mirror being imported (e.g. "
                             "https://deb.debian.org/debian-debug).  Will be used to generate URLs."
                        )
    # TODO: Support never and better time descriptions (like days)
    parser.add_argument('--expiry-valid-for', dest='expiry_valid_for', type=int, default=72,
                        help="How long the URLs will remain valid after being imported. If a file was imported "
                             "in a previous run, its expiry will be reset to match the new expiry time."
                             "(default: %(default)s)"
                        )

    args = parser.parse_args()
    if args.expiry_valid_for < 1:
        print("--expiry-valid-for must >= 1")
        sys.exit(1)

    expiry_time = datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(hours=args.expiry_valid_for)
    for file in args.packages_files:
        import_file(args.vendor, args.external_base_url, expiry_time, file)
