# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core import validators


class BuildId(models.Model):
    build_id = models.CharField(max_length=40, unique=True)

    def __str__(self):
        return self.build_id


class HashType(models.Model):
    hash_name = models.CharField(max_length=12, unique=True)

    def __str__(self):
        return self.hash_name


class FileHash(models.Model):
    hash_value = models.CharField(max_length=64, unique=True)
    hash_type = models.ForeignKey('HashType',
                                  db_index=False,
                                  related_name='+')

    class Meta:
        unique_together = (('hash_value', 'hash_type'),)

    def __str__(self):
        return '%s:%s' % (self.hash_type.hash_name, self.hash_value)


class DownloadUrl(models.Model):
    downloadable_file = models.ForeignKey('DownloadableFile', on_delete=models.CASCADE)
    url = models.TextField(unique=True, validators=[validators.URLValidator()])
    expiry = models.DateTimeField(null=True, blank=True)


class DownloadableFileType(models.Model):
    file_type = models.CharField(max_length=4, unique=True)


class Vendor(models.Model):
    vendor_name = models.TextField(unique=True)


class DownloadableFile(models.Model):
    build_ids = models.ManyToManyField('BuildId')
    file_hashes = models.ManyToManyField('FileHash')
    filename = models.TextField()
    file_size = models.PositiveIntegerField()
    file_type = models.ForeignKey('DownloadableFileType',
                                  db_index=False,
                                  related_name='+')
    vendor = models.ForeignKey('Vendor',
                               db_index=False,
                               related_name='+')

    class Meta:
        unique_together = (('filename', 'vendor'),)
