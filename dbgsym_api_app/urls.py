from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^v1/find-dbgsym(?:/(?P<build_id>[0-9a-f]{40}))?/$', views.find_dbgsym, name='build_id'),
    url(r'^v1/compat/darkserver/buildids(?:/(?P<build_id>[0-9a-f]{40}(?:,[0-9a-f]{40})*))?/$',
        views.darkserver_compat_buildids, name='build_id'),
    url(r'^v1/compat/darkserver/package/(?P<rpm>[^/]+)/?$',
        views.darkserver_compat_package, name='rpm'),
    url(r'^v1/compat/darkserver/rpm2buildids/(?P<rpm>[^/]+)/?$',
        views.darkserver_compat_rpm2buildids, name='rpm'),
    url(r'^v1/compat/darkserver/serverversion$', views.darkserver_compat_serverversion),
]
