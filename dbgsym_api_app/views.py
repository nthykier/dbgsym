from django.shortcuts import resolve_url
from django.http.response import HttpResponseRedirect
from django.db.models import Q

from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import DownloadableFile
from .errors import error_response, E_UNKNOWN_BUILD_ID, E_SERVER_HAS_NO_DATA

import datetime


def download_url_as_payload(download_url):
    payload = {
        'url': download_url.url,
    }
    if download_url.expiry is not None:
        payload['expiry'] = download_url.expiry
    return payload


@api_view(http_method_names=['HEAD', 'GET'])
def find_dbgsym(request, build_id):
    """API to locate a download URL for a file providing the debug symbols for a given ELF build-id

    The build-id should be given at 40 character's hex in the URL.  On success, the client will
    receive a HTTP 200 along with a JSON payload providing download URLs.

    On error, a HTTP 4XX or HTTP 5XX status code will be returned.  The body will be a JSON object
    containing an "error" field (assuming the server is not completely dead).
    """

    now = datetime.datetime.now(datetime.timezone.utc)
    dl_url_query = Q(downloadurl__expiry__isnull=True) | Q(downloadurl__expiry__gt=now)
    if build_id is None:
        # No build-id given, look up a valid one and redirect to that.
        query = DownloadableFile.objects.prefetch_related().\
            filter(dl_url_query, downloadurl__isnull=False, build_ids__build_id__isnull=False)

        downloadable_file = query.first()
        if downloadable_file is None:
            return error_response(E_SERVER_HAS_NO_DATA)
        self_url = resolve_url(find_dbgsym).rstrip('/')
        valid_build_id = downloadable_file.build_ids.first().build_id
        return HttpResponseRedirect("%s/%s/" % (self_url, valid_build_id))

    query = DownloadableFile.objects.prefetch_related().\
        filter(dl_url_query, downloadurl__isnull=False, build_ids__build_id=build_id).\
        order_by('file_size')
    downloadable_files = query.all()
    if downloadable_files is None:
        return error_response(E_UNKNOWN_BUILD_ID, build_id=build_id)

    files = []

    for downloadable_file in downloadable_files:
        download_urls = [x for x in downloadable_file.downloadurl_set.all() if x.expiry is None or now < x.expiry]
        if not download_urls:
            continue
        checksums = {h.hash_type.hash_name: h.hash_value for h in downloadable_file.file_hashes.all()}
        file_payload = {
            'name': downloadable_file.filename,
            'file_type': downloadable_file.file_type.file_type,
            'file_size': downloadable_file.file_size,
            'build_ids': [bid.build_id for bid in downloadable_file.build_ids.all()],
            'download_urls': [download_url_as_payload(x) for x in download_urls],
        }
        if checksums:
            file_payload['checksums'] = checksums
        files.append(file_payload)

    if not files:
        return error_response(E_UNKNOWN_BUILD_ID, build_id=build_id)

    payload = {
        'files': files,
    }

    return Response(payload)


@api_view(http_method_names=['HEAD', 'GET'])
def darkserver_compat_buildids(request, build_id):
    """A *mostly* Fedora Darkserver compatible .../buildids/<...> API point

    Remarks about concrete fields:
      * rpm (can point to a non-RPM)
      * elf (omitted)
    """

    now = datetime.datetime.now(datetime.timezone.utc)
    dl_url_query = Q(downloadurl__expiry__isnull=True) | Q(downloadurl__expiry__gt=now)
    build_ids = build_id.split(',')
    query = DownloadableFile.objects.prefetch_related(). \
        filter(dl_url_query, downloadurl__isnull=False, build_ids__build_id__in=build_ids).\
        order_by('file_size')
    downloadable_files = query.all()

    acceptable_build_ids = set(build_ids)
    emitted_files = set()
    files = []

    if downloadable_files is None:
        return Response(files)

    for downloadable_file in downloadable_files:
        if downloadable_file.filename in emitted_files:
            continue
        emitted_files.add(downloadable_file.filename)
        download_urls = [x for x in downloadable_file.downloadurl_set.all() if x.expiry is None or now < x.expiry]
        if not download_urls:
            continue
        for bid in downloadable_file.build_ids.all():
            if bid.build_id not in acceptable_build_ids:
                continue
            file_payload = {
                'buildid': bid.build_id,
                'rpm': downloadable_file.filename,
                'distro': downloadable_file.vendor.vendor_name,
                'url': download_urls[0].url,
            }
            files.append(file_payload)

    return Response(files)


def darkserver_pkg_lookup(filename):
    now = datetime.datetime.now(datetime.timezone.utc)
    dl_url_query = Q(downloadurl__expiry__isnull=True) | Q(downloadurl__expiry__gt=now)
    filename_query = Q(filename=filename) | Q(filename="%s.rpm" % filename)
    combined_query = dl_url_query & filename_query
    query = DownloadableFile.objects.prefetch_related().\
        filter(combined_query, downloadurl__isnull=False).\
        order_by('file_size')
    all_files = query.all()
    for downloadable_file in all_files:
        download_urls = [x for x in downloadable_file.downloadurl_set.all() if x.expiry is None or now < x.expiry]
        if not download_urls:
            continue
        yield downloadable_file, download_urls


@api_view(http_method_names=['HEAD', 'GET'])
def darkserver_compat_rpm2buildids(request, rpm):
    """A *mostly* Fedora Darkserver compatible .../rpm2buildids/<...> API point

    Remarks about concrete fields:
      * rpm (can point to a non-RPM)
      * elf (omitted)
    """
    results = []
    for downloadable_file, download_urls in darkserver_pkg_lookup(rpm):
        for download_url in download_urls:
            for bid in downloadable_file.build_ids.all():
                results.append({
                    'buildid': bid.build_id,
                    'rpm': downloadable_file.filename,
                    'distro': downloadable_file.vendor.vendor_name,
                    'url': download_url.url,
                })
    return Response(results)


@api_view(http_method_names=['HEAD', 'GET'])
def darkserver_compat_package(request, rpm):
    """A *mostly* Fedora Darkserver compatible .../package/<...> API point
    """
    results = []
    for downloadable_file, download_urls in darkserver_pkg_lookup(rpm):
        results.extend({'url': x.url} for x in download_urls)
    return Response(results)


@api_view(http_method_names=['HEAD', 'GET'])
def darkserver_compat_serverversion(request):
    """A *mostly* Fedora Darkserver compatible .../serverversion/ API point
    """
    return Response({'server-version': '2.0'})
