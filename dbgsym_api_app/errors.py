import collections
import logging

from django.conf import settings
from rest_framework.response import Response


logger = logging.getLogger(__name__)


E_UNKNOWN_BUILD_ID = 1
E_INTERNAL_ERROR = 2
E_PAYLOAD_NOT_VALID_JSON = 3
# E_REQUEST_MISSING_REQUIRED_FIELD = 4
# E_REQUEST_PAYLOAD_WRONG_TYPE = 5
E_SERVER_HAS_NO_DATA = 6


DbgsymErrorCode = collections.namedtuple('DbgsymErrorCode', ['default_status_code', 'code_name', 'message_template'])


ERRORS = {
    E_UNKNOWN_BUILD_ID: DbgsymErrorCode(404, 'E_UNKNOWN_BUILD_ID', 'Unknown build-id ID %(build_id)s'),
    E_INTERNAL_ERROR: DbgsymErrorCode(500, 'E_INTERNAL_ERROR', 'Internal error: %(message)s'),
    E_PAYLOAD_NOT_VALID_JSON: DbgsymErrorCode(400, 'E_PAYLOAD_NOT_VALID_JSON',
                                              'Request cannot be parsed as valid UTF-8 encoded JSON'),
    # E_REQUEST_MISSING_REQUIRED_FIELD: DbgsymErrorCode(400, 'E_REQUEST_MISSING_REQUIRED_FIELD',
    #                                                   'Request is missing required field %(field_name)s'),

    # E_REQUEST_PAYLOAD_WRONG_TYPE: DbgsymErrorCode(400, 'E_REQUEST_WRONG_TYPE',
    #                                               'Request payload had wrong type. Expected JSON %(json_type)s'),
    E_SERVER_HAS_NO_DATA: DbgsymErrorCode(503, 'E_SERVER_HAS_NO_DATA',
                                          'The server appears to have no data to answer the request'),
}


def error_response(code, status_code=None, **kwargs):
    e = ERRORS.get(code)
    message = None
    if e is not None:
        try:
            message = e.message_template % kwargs
        except RuntimeError as ex:
            if getattr(settings, 'DEBUG', False):
                raise
            logger.error("Invalid kwargs for error code %s %s format: %s", e.code_name, code, str(ex))
            e = None
            message = None
        else:
            if not status_code:
                status_code = e.default_status_code
    if e is None:
        e = ERRORS[E_INTERNAL_ERROR]
        status_code = 500
        message = e.message_template % {'message': 'Error while generating the real error message'}
    payload = {
        'error': {
            'code': code,
            'code_name': e.code_name,
            'message': message,
        }
    }
    return Response(payload, status=status_code)
