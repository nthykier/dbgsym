from django.apps import AppConfig


class DbgsymApiAppConfig(AppConfig):
    name = 'dbgsym_api_app'
