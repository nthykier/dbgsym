def fetch_or_create(cls, **kwargs):
    return cls.objects.get_or_create(**kwargs)[0]


def bulk_import_hash(cls, all_hashes, field_name, create_args=None, filter_args=None):
    hash_table = {}
    fargs = {
        "%s__in" % field_name: all_hashes
    }
    if filter_args is not None:
        fargs.update(filter_args)
    existing_entries = cls.objects.filter(**fargs)
    for entry in existing_entries:
        hash_table[getattr(entry, field_name)] = entry
    to_create = []
    for x in all_hashes:
        if x in hash_table:
            continue
        cargs = {
            field_name: x
        }
        if create_args is not None:
            cargs.update(create_args)
        to_create.append(cls(**cargs))

    cls.objects.bulk_create(to_create)
    for entry in to_create:
        hash_table[getattr(entry, field_name)] = entry

    return hash_table
